import telebot
import logging
import os
import tools


############################################################
"""
Setup the bot with the token given by the BotFather
obs: the token is stored in an evironment variable
"""
token = os.environ['BOT_TOKEN']

bot = telebot.AsyncTeleBot(token)
logger = telebot.logger
telebot.logger.setLevel(logging.DEBUG)


############################################################
"""
Basic commands
- /start
- /help
"""
@bot.message_handler(commands=['start'])
def start(message):
    bot.reply_to(message,"Essa é uma mensagem inicial! Esta ação está no gatilho do comando '/start'")

@bot.message_handler(commands=['help'])
def help(message):
    bot.reply_to(message,tools.help_text)


############################################################
"""
If any message match with the regexp, so it might be a question,
bot then replies it with the answer
"""
@bot.message_handler(regexp=".*\?")
def answer(message):
    bot.reply_to(message,"42")


############################################################
"""
If any message is palindrome, bot then calls the attention to that
"""
@bot.message_handler(func=tools.palindromo)
def nota_palindromo(message):
    bot.reply_to(message, "Notou algo engraçado sobre essa mensagem? Tente ler de trás pra frente... (dica: use '/inverte' com ela)")


@bot.message_handler(commands=['inverte'])
def inverte(message):
    text = " ".join(message.text.split(' ')[1:])
    bot.reply_to(message,text[::-1])


############################################################
"""
Name manipulation and storage
- /salvanome stores it in a json file with chat id
- /peganome fetches the json file and sends it to the user
"""
@bot.message_handler(commands=['salvanome'])
def pega_nome(message):
    if tools.tem_param(message.text):
        n = tools.extrai_nome(message)
        tools.salva_nome(message.chat.id, n)
        bot.reply_to(message, "Salvei seu nome, " + n['nome'])
    else:
        bot.reply_to(message, "Você precisa me informar um nome")

@bot.message_handler(commands=['peganome'])
def recupera_nome(message):
    nome = tools.fetch_nome(message)
    bot.reply_to(message,"Você é {nome}".format(nome=nome['nome']))


############################################################
"""
Beer API interaction
- /beer asks the user for beer infos, set a keyboard to the reply
and marks the chat id as waiting for an answer
- if the user is in the waiting list, then sends a beer info fetched
from the api and sends it to the user
"""

@bot.message_handler(commands=['breja'])
def ask_quote(message):
    cid = message.chat.id
    bot.send_message(cid,"Quer uma informação de cerveja?", reply_markup=tools.beerSelect)
    tools.beerQueue[cid] = 1

@bot.message_handler(func=tools.enqueued)
def send_quote(message):
    cid = message.chat.id
    text = message.text

    if text == 'sim':
        bot.reply_to(message,tools.random_beer(), reply_markup=tools.hideBoard)
        tools.beerQueue[cid] = 0
    elif text == 'não':
        bot.reply_to(message, "Oops, desculpe...", reply_markup=tools.hideBoard)
        tools.beerQueue[cid] = 0
    else:
        bot.reply_to(message, "Não entendi... Tente de novo")


bot.polling()