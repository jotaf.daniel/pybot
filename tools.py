import telebot
import requests
import json

help_text = """
Comandos:
/start -> inicia a interação com o bot
/help -> mostra esta lista de comandos
/inverte -> inverte a ordem dos caractéres na mensagem
/salvanome <nome> -> armazena o <nome> passado
/peganome -> recupera o nome armazenado
/breja -> pergunta se deseja informações sobre cerveja
"""

def tem_param(m):
    return len(m.split(' ')) > 1

def palindromo(message):
    text = message.text.lower()
    text = text.replace(' ', '')
    return text == text[::-1]

def extrai_nome(message):
    aux = message.text.split(' ')[1:]
    nome = " ".join(aux)
    return dict(nome=nome)

def salva_nome(id,n):
    with open("name{}".format(id),'w') as f:
        f.write(json.dumps(n))
    f.close

def fetch_nome(message):
    filename = "name{}".format(message.chat.id)
    try:
        f = open(filename)
    except FileNotFoundError:
        nome = {'nome': "desconhecido por enquanto, identifique-se com /salvanome para acessar essa função corretamente"}
    else:
        nome = json.loads(f.read())
        f.close()

    return nome


beerQueue = {}
beerSelect = telebot.types.ReplyKeyboardMarkup(one_time_keyboard=True)
beerSelect.add('sim','não')
hideBoard = telebot.types.ReplyKeyboardRemove()

def beer_to_text(beer):
    return """
--- {name} ---
{desc}
""".format(name=beer['name'], desc=beer['description'])

def random_beer():
    res = requests.get("https://api.punkapi.com/v2/beers/random")
    return beer_to_text(json.loads(res.text)[0])

def enqueued(m):
    return m.chat.id in beerQueue.keys() and beerQueue[m.chat.id] == 1